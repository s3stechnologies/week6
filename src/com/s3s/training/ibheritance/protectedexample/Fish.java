package com.s3s.training.ibheritance.protectedexample;

import com.s3s.training.inheritance.example.Animal;

public class Fish {
	
	Animal animal = new Animal();
	
	public void doNothing() {
		//animal.printName("Biwash", 12); // protected method cannot access in different package
	}


}
