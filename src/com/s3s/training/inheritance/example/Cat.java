package com.s3s.training.inheritance.example;

public class Cat extends Animal{ // parent child, inheritance
	
	String type;
	String attitude;
	
	
	public Cat() {
		
	}
	
	public Cat(String type) {
		super(type, "Black", 90);
		System.out.println("I'm in Cat constructor! ....");
		
		
		
	}
	
	
	@Override
	public Cat printName(String name, int id) {
		
		// code here can be different
		return new Cat();       //accesing variables
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((attitude == null) ? 0 : attitude.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Cat other = (Cat) obj;
		if (attitude == null) {
			if (other.attitude != null)
				return false;
		} else if (!attitude.equals(other.attitude))
			return false;
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!type.equals(other.type))
			return false;
		return true;
	}

	
	
	
	
	

}
