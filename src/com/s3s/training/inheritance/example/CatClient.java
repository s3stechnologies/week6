package com.s3s.training.inheritance.example;

public class CatClient {
	
	
	public static boolean isTrue() {
		Cat dog = new Cat();
		
		//dog.hashCode();
		
		Cat puppy = new Cat();
		
	//	puppy.hashCode();
		
		return dog.equals(puppy);
		
	}
	
	public static int divideTwoNumber() {
		int a = 0;
		int b = 12;
		
		return b/a;
	
	}

	public static void main(String[] args) {
		
		
		
		
	//	Animal animal = new Cat();
		//Cat cat = new Cat("Cat");
	//	System.out.println(animal.printName());
		
	//	Object obj = new Object();
		
		
		Animal animal = new Animal();
		
		Cat cat = new Cat();
		
		//Cat puppy = new Cat();
		
		Object obj = new Object();
		
		boolean isInstanceOf = (cat instanceof Animal) ;
		
		System.out.println(isInstanceOf);
		
		
		
		
		
		
	//	System.out.println(puppy.equals(cat));
		
	//	System.out.println(puppy.hashCode());
		
		
		
		System.out.println(isTrue());
		
		
		//System.out.println(dog.hashCode());
		
		
	//	System.out.println(dog.printName());
		
		// IS-A
		
	//	int a = 109.90;
		
	//	int b = 200;
		
		
		//int c = divideTwoNumber();
		

		//System.out.print(c);
		
	//	Cat cat = (Cat) new Animal();
		
		
	//	Animal cat1 = new Cat();
		
	//	Animal dog = new Dog();
		
	//	Dog puppy = (Dog) new Animal();
		
		
		
		
		

	}

}
