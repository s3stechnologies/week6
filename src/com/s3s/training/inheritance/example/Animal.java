package com.s3s.training.inheritance.example;

public class Animal {
	
	String name ;
	String color;
	String gender;
	double weight;
	

	public Animal() {
		
		System.out.println("I'm in Animal default constructor! ....");
	}
	
	
	public Animal(String name) {
		this.name = name ;
	
		System.out.println("I'm in Animal constructor with name parameter! ....");
		
	}
	
	
	public Animal(String name, String color) {
		this(name);
		System.out.println("I'm in Animal constructor with 2 parameter! ....");
		
	}
	
	public Animal(String name, String color, int age) {
		this(name);
		System.out.println("I'm in Animal constructor with 2 parameter! ....");
		
	}
	
	
	protected Object printName(String name, int id) {
		
		return new CatClient();
	}
	


}
